package demo.practice.practice;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import demo.practice.practice.Model.LoginResponse;
import demo.practice.practice.Registration.LoginActivity;
import demo.practice.practice.Splash.SplaseActivity;
import demo.practice.practice.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity {
    ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.setPresenter(this);
        getUserData();
    }

    public void getUserData() {
        /**
         * It will observe database changes untill activity to be finished
         */
        /*LiveData<List<LoginResponse>> liveData;
        liveData = categoryDatabase.loginDao().fetchAllDataLive();
        liveData.observe(this, new Observer<List<LoginResponse>>() {
            @Override
            public void onChanged(@Nullable final List<LoginResponse> loginResponses) {
                if(loginResponses.size() > 0) {
                    LoginResponse loginResponse = loginResponses.get(0);

                    mBinding.txtuser.setText("" + loginResponse.userId + "\n" +
                            loginResponse.email + "\n" +
                            loginResponse.firstName + "\n" +
                            loginResponse.lastName + "\n"
                    );
                }
            }
        });*/


        /**
         * It will fetch user data from database once
         */
        List<LoginResponse> loginResponseList = categoryDatabase.loginDao().fetchAllData();
        if(loginResponseList.size() > 0) {
            LoginResponse loginResponse = loginResponseList.get(0);

            mBinding.txtuser.setText("" + loginResponse.userId + "\n" +
                    loginResponse.email + "\n" +
                    loginResponse.firstName + "\n" +
                    loginResponse.lastName + "\n"
            );
        }

    }
    public void onclickLogout() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                categoryDatabase.loginDao().nukeTable();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                       LoginActivity.pushActivity(MainActivity.this);
                    }
                });
            }
        }).start();
    }
}
