package demo.practice.practice.Registration;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import java.util.Random;

import demo.practice.practice.BaseActivity;
import demo.practice.practice.Common.ValidationUtils;
import demo.practice.practice.MainActivity;
import demo.practice.practice.Model.LoginResponse;
import demo.practice.practice.R;
import demo.practice.practice.databinding.ActivityLoginBinding;
import demo.practice.practice.databinding.ActivityRegistrationBinding;

public class LoginActivity extends BaseActivity {

    public static void pushActivity(Activity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    ActivityLoginBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mBinding.setPresenter(this);
    }

    public void onclickLogin() {
        if (ValidationUtils.isVauleFilled(mBinding.edtemail, getString(R.string.val_email), this, mBinding.getRoot()) &&
                ValidationUtils.isVauleFilled(mBinding.edtlpassword, getString(R.string.val_password), this, mBinding.getRoot())
                ) {
            Random r = new Random();
            int i1 = r.nextInt(1000 - 1) + 1;

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.userId = ""+i1;
            loginResponse.email = mBinding.edtemail.getText().toString().trim();
            loginResponse.firstName = "";
            loginResponse.lastName = "";

            saveUserData(loginResponse);

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void onclickSignup() {
        RegistrationActivity.pushActivity(this);
    }

    /**
     * Save SingleData.
     *
     * @param loginResponse
     */
    public void saveUserData(final LoginResponse loginResponse) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                categoryDatabase.loginDao().insertOnlySingleRecord(loginResponse);
            }
        }).start();
    }


    /**
     * Save Multiple Data
     *
     * @param loginResponse
     */
    /*public void saveUserData(final List<LoginResponse> loginResponse) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                categoryDatabase.loginDao().insertMultipleListRecord(loginResponse);
            }
        }).start();
    }*/


}
