package demo.practice.practice.Registration;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import java.util.List;
import java.util.Random;

import demo.practice.practice.BaseActivity;
import demo.practice.practice.Common.Utils;
import demo.practice.practice.Common.ValidationUtils;
import demo.practice.practice.MainActivity;
import demo.practice.practice.Model.LoginResponse;
import demo.practice.practice.R;
import demo.practice.practice.databinding.ActivityRegistrationBinding;

public class RegistrationActivity extends BaseActivity {

    public static void pushActivity(Activity activity) {
        Intent intent = new Intent(activity, RegistrationActivity.class);
        activity.startActivity(intent);
    }

    ActivityRegistrationBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_registration);
        mBinding.setPresenter(this);
    }

    public void onclickClose() {
        finish();
    }

    public void onclickSignup() {
        if (ValidationUtils.isVauleFilled(mBinding.edtemail, getString(R.string.val_email), this, mBinding.getRoot()) &&
                ValidationUtils.isVauleFilled(mBinding.edtfname, getString(R.string.val_fname), this, mBinding.getRoot()) &&
                ValidationUtils.isVauleFilled(mBinding.edtlname, getString(R.string.val_lname), this, mBinding.getRoot()) &&
                ValidationUtils.isVauleFilled(mBinding.edtlpassword, getString(R.string.val_password), this, mBinding.getRoot())
                ) {

            Random r = new Random();
            int i1 = r.nextInt(1000 - 1) + 1;

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.userId = ""+i1;
            loginResponse.email = mBinding.edtemail.getText().toString().trim();
            loginResponse.firstName = mBinding.edtfname.getText().toString().trim();
            loginResponse.lastName = mBinding.edtlname.getText().toString().trim();

            saveUserData(loginResponse);

            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }
    }

    /**
     * Save SingleData in RoomDatabse.
     *
     * @param loginResponse
     */
    public void saveUserData(final LoginResponse loginResponse) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                categoryDatabase.loginDao().insertOnlySingleRecord(loginResponse);
            }
        }).start();
    }


    /**
     * Save Multiple Data
     *
     * @param loginResponse
     */
    /*public void saveUserData(final List<LoginResponse> loginResponse) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                categoryDatabase.loginDao().insertMultipleListRecord(loginResponse);
            }
        }).start();
    }*/


}
