package demo.practice.practice.Databse.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import demo.practice.practice.Model.DetailResponse;
import demo.practice.practice.Model.DetailResponse;


@Dao
public interface DaoDetail {

    @Insert
    void insertMultipleRecord(DetailResponse... detailResponses);

    @Insert
    void insertMultipleListRecord(List<DetailResponse> detailResponses);

    @Insert
    void insertOnlySingleRecord(DetailResponse detailResponse);


    @Query("SELECT * FROM DetailResponse")
    LiveData<List<DetailResponse>> fetchAllDataLive();

    @Query("SELECT * FROM DetailResponse")
    List<DetailResponse> fetchAllData();

    @Query("SELECT * FROM DetailResponse WHERE uniqueId =:detailResponse_uniqueId")
    List<DetailResponse> fetchDatacategorywise(String detailResponse_uniqueId);

    @Query("SELECT * FROM DetailResponse WHERE uniqueId =:detailResponse_uniqueId")
    LiveData<List<DetailResponse>> fetchDatacategorywiseLive(String detailResponse_uniqueId);

    @Query("SELECT * FROM DetailResponse WHERE userId =:detailResponse_userId")
    DetailResponse getSingleRecord(int detailResponse_userId);

    @Update
    void updateRecord(DetailResponse detailResponse);

    @Delete
    void deleteRecord(DetailResponse detailResponse);

    @Query("DELETE FROM DetailResponse")
    public void nukeTable();

}