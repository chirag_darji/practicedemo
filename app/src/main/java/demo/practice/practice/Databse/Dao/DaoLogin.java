package demo.practice.practice.Databse.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import demo.practice.practice.Model.LoginResponse;


@Dao
public interface DaoLogin {

    @Insert
    void insertMultipleRecord(LoginResponse... loginResponses);

    @Insert
    void insertMultipleListRecord(List<LoginResponse> loginResponses);

    @Insert
    void insertOnlySingleRecord(LoginResponse loginResponse);


    @Query("SELECT * FROM LoginResponse")
    LiveData<List<LoginResponse>> fetchAllDataLive();

    @Query("SELECT * FROM LoginResponse")
    List<LoginResponse> fetchAllData();

    @Query("SELECT * FROM LoginResponse WHERE uniqueId =:loginResponse_uniqueId")
    List<LoginResponse> fetchDatacategorywise(String loginResponse_uniqueId);

    @Query("SELECT * FROM LoginResponse WHERE uniqueId =:loginResponse_uniqueId")
    LiveData<List<LoginResponse>> fetchDatacategorywiseLive(String loginResponse_uniqueId);

    @Query("SELECT * FROM LoginResponse WHERE userId =:loginResponse_userId")
    LoginResponse getSingleRecord(int loginResponse_userId);

    @Update
    void updateRecord(LoginResponse loginResponse);

    @Delete
    void deleteRecord(LoginResponse loginResponse);

    @Query("DELETE FROM LoginResponse")
    public void nukeTable();

}