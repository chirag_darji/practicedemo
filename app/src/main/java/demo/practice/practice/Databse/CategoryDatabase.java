package demo.practice.practice.Databse;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import demo.practice.practice.Databse.Dao.DaoDetail;
import demo.practice.practice.Databse.Dao.DaoLogin;
import demo.practice.practice.Model.DetailResponse;
import demo.practice.practice.Model.LoginResponse;
import demo.practice.practice.Model.TypeConverter.ItemConverters;

@Database(entities = {LoginResponse.class , DetailResponse.class}, version = 1,exportSchema = false)
@TypeConverters({ItemConverters.class})
public abstract class CategoryDatabase extends RoomDatabase {

    private static CategoryDatabase INSTANCE;

    public abstract DaoLogin loginDao();
    public abstract DaoDetail detailDao();

    public static CategoryDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), CategoryDatabase.class, "practice-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
