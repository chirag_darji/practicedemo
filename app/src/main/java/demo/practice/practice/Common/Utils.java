package demo.practice.practice.Common;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;



import org.w3c.dom.CharacterData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;


import demo.practice.practice.BaseActivity;
import demo.practice.practice.Model.LoginResponse;
import demo.practice.practice.R;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by umang on 29/11/16.
 */

public class Utils {

    private static final String TAG = "Utils";
    public static String mCurrentPhotoPath;
    private static ProgressDialog mProgressDialog;
    private static HashMap<String, String> mHeaderMap;

    // CR
    public static String mCurrentPickedPhotoPath;
    public static String mCurrentPhotoThumbPath;
    public static String imageFileName;
    public static File imageFile = null;
    public static File imageThumbFile = null;
    public static Bitmap imageBitmap;
    public static int mediaWidth = 0, mediaHeight = 0;
    public static double imageSize = 0D;
    public static double mediaThumbSize = 0D;


    static Uri capturedVideoUri;
    static String capturedVideoFileName = "";
    static String videoFileName = "";
    static String videoThumbFileName = "";
    static File capturedVideoFile = null;
    static File videoFile = null;
    static File videoThumbFile = null;
    public static String capturedVideoPath = "", videoFilePath = "", videoThumbFilePath = "";
    public static double videoSize = 0D;
    public static double mediaDuration = 0D;
    public static boolean isCompressionSuccess = false;
    public static AlertDialog compressDialog = null;


    public static void hideSoftKeyboard(BaseActivity context) {
        if (context.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }


    public static void openKeyboard(View view, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static String generateChatId(String myUserId, String userId) {
        int myId = Integer.parseInt(myUserId);
        int otherId = Integer.parseInt(userId);
        if (myId < otherId) {
            return TextUtils.concat(myUserId, "_", userId).toString();
        } else {
            return TextUtils.concat(userId, "_", myUserId).toString();
        }

    }

    public static void showMessage(String message, View view, Context context) {
       /* Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(4);

        snackbar.show();*/

        showToast(message,context);
    }

    public static void showToast(String message, Context c) {
        Toast.makeText(c, message, Toast.LENGTH_SHORT).show();
    }



    public static File saveToInternalStorage(Bitmap bitmapImage, Context context) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("wallpaperDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "wallpaper.png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath;
    }



  /*  public static ProfileData getProfileDetails(BaseActivity activity) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            // String json = SPreferences.getInstance(activity).getStringValue(SPreferences.PROFILE_DATA);
            ProfileData obj = mapper.readValue(SPreferences.getInstance(activity).getStringValue(SPreferences.PROFILE_DATA), ProfileData.class);
            return obj;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }*/

    public static File createTempFile(Context mContext, String mExtension, Bitmap image) {
        if (image != null) {
            FileOutputStream out = null;
            File file = null;
            try {

                file = File.createTempFile(
                        "s3_temp_file_",
                        mExtension,
                        mContext.getCacheDir());
                out = new FileOutputStream(file);
                if (image.getRowBytes() * image.getHeight() > 100000) {
                    image.compress(Bitmap.CompressFormat.JPEG, 70 /*ignored for PNG*/, out);
                } else {
                    image.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, out);
                }
                //image.compress(Bitmap.CompressFormat.JPEG, 70 /*ignored for PNG*/, out);
                out.flush();
            } catch (IOException e) {
                Log.e(TAG, "", e);
            }
            return file;
        } else {
            return null;
        }

    }


    public static String getFileName(String filePath) {
        if (!TextUtils.isEmpty(filePath)) {
            Log.i(TAG, "file path of others image " + filePath);
            return filePath.substring(filePath.lastIndexOf("/") + 1);
        } else {
            return "";
        }
    }


    public static File copyFile(Uri data, Context context) {

        String outputfile = context.getCacheDir() + getFileName(data.getPath());

        InputStream in = null;
        try {
            in = context.getContentResolver().openInputStream(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            File f = new File(outputfile);
            f.setWritable(true, false);
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while ((length = in.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.close();
            in.close();
            return f;
        } catch (IOException e) {
            System.out.println("error in creating a file");
            e.printStackTrace();
        }
        return null;

    }

    private static Intent getImagePicker() {
        return (new Intent("android.intent.action.GET_CONTENT")).setType("image/*");
    }


    public static File createImageFile() throws IOException {

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = new File(
                storageDir + File.separator + imageFileName + ".jpg"
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    public static void showMessageLongDur(String message, View view, Context context) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.cardview_dark_background));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(4);

        snackbar.show();
    }


    public static void showProgressDialog(BaseActivity mActivity, String message, boolean isCancellable) {
        mProgressDialog = new ProgressDialog(mActivity);

        mProgressDialog.setMessage(message);
        mProgressDialog.setTitle("");
        mProgressDialog.setCancelable(isCancellable);
        mProgressDialog.show();
    }

    public static void hideProgrssDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    public static int getHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }


    public static int getWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static Bitmap decodeFile(File f, int RequiredSize) {
        int orientation;
        if (f == null) {
            return null;
        }
        try {
// Decode image size

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

// The new size we want to scale to
            final int REQUIRED_SIZE = RequiredSize;

// Find the correct scale value. It should be the power of 2.
            int scale = 1;

            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

// Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            orientation = getCameraPhotoOrientation(f.getAbsolutePath());

            bitmap = RotateBitmap(bitmap, orientation);

            try {
                FileOutputStream out = new FileOutputStream(f);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return bitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static int getCameraPhotoOrientation(String filepath) {
        int rotate = 0;
        try {
            ExifInterface exif = new ExifInterface(filepath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
// TODO: handle exception
        }
        return rotate;
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    public static String formatTimeINMMSSAA(long timstamp) {
        SimpleDateFormat format = new SimpleDateFormat("HH:MM aaa");

        Calendar calender = Calendar.getInstance();
        calender.setTimeZone(TimeZone.getDefault());
        calender.setTimeInMillis(timstamp);

        String dateFormmated = format.format(calender.getTime());
        return dateFormmated;
    }


    public static String utcToFormattedTime(long timestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.setTimeInMillis(timestamp);

        DateFormat df = new SimpleDateFormat("hh:mm a");
        df.setTimeZone(TimeZone.getDefault());
        return df.format(cal.getTime());
    }


    public static String utcToFormattedTimeWithDate(long timestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.setTimeInMillis(timestamp);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        df.setTimeZone(TimeZone.getDefault());
        return df.format(cal.getTime());
    }


    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static String utcMillisToCurrentTime(long millies) {

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.setTimeInMillis(millies);
        Date date = cal.getTime();

        DateFormat sdf2 = new SimpleDateFormat("hh:mm aaa");
        sdf2.setTimeZone(TimeZone.getDefault());
        return sdf2.format(date);


    }

    // CR
    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        if (context != null) {
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }


    // CR
    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    public static String getStringFormattedDuration(double milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        //      return  String.format("%02d Min, %02d Sec",
        //                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
        //                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
        //                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));

        // return timer string
        return finalTimerString;
    }

    // CR
    public static String getStringFormattedMediaSize(double mediaSize) {
        String sSize = "1 KB";
        try {
            if (mediaSize > 0) {
                float fSize = Float.valueOf(String.format("%.1f", ((float)mediaSize / (1024))));
                if (fSize < 999.0D) {
                    sSize = fSize + "KB";
                } else {
                    fSize = Float.valueOf(String.format("%.1f", ((float)mediaSize / (1048576))));
                    if (fSize < 999.0D) {
                        sSize = fSize + "MB";
                    } else {
                        fSize = Float.valueOf(String.format("%.1f", ((float)mediaSize / (1073741824))));
                        if (fSize < 999.0D) {
                            sSize = fSize + "GB";
                        } else {
                            sSize = ">999 GB";
                        }
                    }
                }
            }/* else { sSize = "1 KB"; }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sSize;
    }

    // CR
    public static void copyFile(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }


    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    @NonNull
    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }

    @NonNull
    public static RequestBody createPartFromStringText(String descriptionString) {
        return RequestBody.create(MediaType.parse("text/plain"), descriptionString);
    }

    @NonNull
    // public static MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
    public static MultipartBody.Part prepareFilePart(String partName, String imageFilePath, Context context) {
        // Utils.showLog("30_Dec_2016", "--->   Preparing File Part From: " + imageFilePath);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        // File file = FileUtils.getFile(this, fileUri);
        File file = new File(imageFilePath);

        // Utils.showLog("30_Dec_2016", "--->   Prepared File Path: " + file.getAbsolutePath());

        // create RequestBody instance from file
        /*RequestBody requestFile =
                RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file);*/

        /*Uri fileUri = null;
        if (Build.VERSION.SDK_INT >= 24) {
            fileUri = FileProvider.getUriForFile(context,
                    context.getApplicationContext().getPackageName() + ".provider", file);
        } else {
            fileUri = Uri.fromFile(file);
        }
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(context.getContentResolver().getType(fileUri)),
                        file
                );*/

        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        // LogC.i("--->   mimeType:", "--->   mimeType: " + mimeType); // mimeType: image/jpeg
        RequestBody requestFile = RequestBody.create(MediaType.parse(mimeType),file);


        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }


    /**
     * Checking keyboard height and keyboard visibility
     */
    public static final int MIN_KEYBOARD_HEIGHT_PX = 150;
    public static void checkKeyboardHeight(final View parentLayout,final KeyboardVisibilityListener listener) {

        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    private final Rect windowVisibleDisplayFrame = new Rect();
                    private int lastVisibleDecorViewHeight;

                    @Override
                    public void onGlobalLayout() {
                        // Retrieve visible rectangle inside window.
                        parentLayout.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame);
                        final int visibleDecorViewHeight = windowVisibleDisplayFrame.height();

                        // Decide whether keyboard is visible from changing decor view height.
                        if (lastVisibleDecorViewHeight != 0) {
                            if (lastVisibleDecorViewHeight > visibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX) {
                                // Calculate current keyboard height (this includes also navigation bar height when in fullscreen mode).
                                int currentKeyboardHeight = parentLayout.getHeight() - windowVisibleDisplayFrame.bottom;
                                // Notify listener about keyboard being shown.
                                listener.onKeyboardShown(currentKeyboardHeight);
                            } else if (lastVisibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX < visibleDecorViewHeight) {
                                // Notify listener about keyboard being hidden.
                                listener.onKeyboardHidden();
                            }
                        }
                        // Save current decor view height for the next call.
                        lastVisibleDecorViewHeight = visibleDecorViewHeight;
                    }
                });

    }

    public interface KeyboardVisibilityListener {

        void onKeyboardShown(int currentKeyboardHeight);
        void onKeyboardHidden();
    }



/*
    public static boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }*/

}
