package demo.practice.practice.Common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


@SuppressLint("SimpleDateFormat")
public class CommonWidget {
    public static ProgressDialog dialog;

    public static void showAlertDialog(final Context context, String title,
                                       String message, final boolean isfinish) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(title);
        builder1.setMessage(message);
        builder1.setCancelable(false);
        builder1.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if (isfinish) {
                            ((Activity) context).finish();
                        }
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.setCanceledOnTouchOutside(false);
        alert11.show();
    }

    public static void showAlertDialog(final Context context, String title,
                                       String message, DialogInterface.OnClickListener dialogInterfaceCancel, DialogInterface.OnClickListener dialogInterfaceOK) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(title);
        builder1.setMessage(message);
        builder1.setCancelable(false);
        builder1.setNeutralButton(android.R.string.ok, dialogInterfaceOK);
        builder1.setNegativeButton(android.R.string.cancel, dialogInterfaceCancel);

        AlertDialog alert11 = builder1.create();
        alert11.setCanceledOnTouchOutside(false);
        alert11.show();
    }

    public static void showProgressDialog(Context context, String message) {

        dialog = new ProgressDialog(context);
        //dialog.setProgressStyle(android.R.style.Theme_Material_Dialog);
        dialog.setMessage(message);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void showMsg(Context context, String message) {

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void dismissProgressDialog() {


        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Date getDateFromString(String date, String dateFormat) {
        Date dateValue = new Date();
        try {
            SimpleDateFormat fmtOut = new SimpleDateFormat(dateFormat);
            dateValue = fmtOut.parse(date);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dateValue;
    }
    public static String stringForTime1(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        Formatter mFormatter = new Formatter();
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
        // return mFormatter.format("%02d:%02d:%02d", hours, minutes, seconds).toString();
    }
    public static String stringForTime2(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        Formatter mFormatter = new Formatter();

        if(hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else if(minutes > 0) {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d", seconds).toString();
        }
        // return mFormatter.format("%02d:%02d:%02d", hours, minutes, seconds).toString();
    }

    public static String simpleDateFormate1(Date date, String dateFormat) {

        SimpleDateFormat fmtOut = new SimpleDateFormat(dateFormat);
        return fmtOut.format(date);
    }

    public synchronized static Date impreciseParseDate(String encoded) {
        try {
            return impreciseDateFormat.parse(encoded);
        } catch (ParseException e) {
            // Should never happen
            Log.d("Tenderfoot", "could not parse date: " + encoded);
            return null;
        }
    }

    public static final DateFormat impreciseDateFormat;

    static {
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        format.setTimeZone(new SimpleTimeZone(0, "GMT"));
        impreciseDateFormat = format;
    }

    public static String getCurrentAddress(double latitude, double longitude,
                                           Context context) {
        String finalAddress = "";
        Log.e("lat->", "" + latitude + "    lng->" + longitude);
        if (latitude != 0.0 && longitude != 0.0) {
            Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
            StringBuilder builder = new StringBuilder();

            try {
                List<Address> address = geoCoder.getFromLocation(latitude,
                        longitude, 1);
                if (address.size() > 0) {
                    int maxLines = address.get(0).getMaxAddressLineIndex();
                    for (int i = 0; i < maxLines; i++) {
                        String addressStr = address.get(0).getAddressLine(i);
                        builder.append(addressStr);
                        builder.append(" ");
                    }

                    finalAddress = builder.toString(); // This is the complete
                    // address.
                } else {
                    finalAddress = "";
                }
            } catch (IOException e) {

            } catch (NullPointerException e) {

            }
        }
        return finalAddress;
    }

    public static Address geocoding(String strAddress, Context context) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            return location;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCurrentCity(double latitude, double longitude,
                                        Context context) {
        String fnialAddress = "";
        Log.e("lat->", "" + latitude + "    lng->" + longitude);
        if (latitude != 0.0 && longitude != 0.0) {

            try {

                // Geocoder geoCoder = new Geocoder(context,
                // Locale.getDefault());

                Geocoder geoCoder = new Geocoder(context, Locale.getDefault());

                List<Address> address = geoCoder.getFromLocation(latitude,
                        longitude, 1);
                if (address != null && address.size() > 0)
                    fnialAddress = address.get(0).getAdminArea() + ","
                            + address.get(0).getCountryName(); // This is the
                // complete
                // address.
            } catch (IOException e) {

            } catch (NullPointerException e) {

            }
        }
        return fnialAddress;
    }

    public static String GenerateUUID() {
        //
        // Creating a random UUID (Universally unique identifier).
        //
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString().toUpperCase(Locale.ENGLISH);

        return randomUUIDString;
    }

    public static String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        try {
            month = month.substring(0, 3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return month;
    }

    public static String getMonthForIntFull(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }

        return month;
    }

    public static Drawable getTintedDrawable(Resources res, int drawableResId, int colorResId) {
        Drawable drawable = res.getDrawable(drawableResId);
        int color = res.getColor(colorResId);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return drawable;
    }



    /*get Devide Height and Width*/


    public static int getHeight(Context context) {

        try {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int height = displaymetrics.heightPixels;
            Log.e("height", "" + height);
            return height;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getwidth(Context context) {
        try {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            Log.e("width", "" + width);
            return width;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void convertToClearableEditText(Context context, final EditText edit, int clearIcon) {

        final Drawable xD = context.getResources().getDrawable(clearIcon);

        if (edit != null) {

            edit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    setClearIconVisible(!TextUtils.isEmpty(edit.getText().toString().trim()), edit, xD);
                }
            });
            edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        setClearIconVisible(!TextUtils.isEmpty(edit.getText().toString().trim()), edit, xD);
                    } else {
                        setClearIconVisible(false, edit, xD);
                    }
                }
            });
            edit.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (edit.getCompoundDrawables()[2] != null) {
                        boolean tappedX = event.getX() > (edit.getWidth() - edit.getPaddingRight() - xD.getIntrinsicWidth());
                        if (tappedX) {
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                edit.setText("");
                            }
                            return true;
                        }
                    }
                    return false;
                }
            });

        }

    }

    private static void setClearIconVisible(boolean visible, final EditText edit, Drawable xD) {
        boolean wasVisible = (edit.getCompoundDrawables()[2] != null);
        if (visible != wasVisible) {
            Drawable x = visible ? xD : null;
            edit.setCompoundDrawablesWithIntrinsicBounds(edit.getCompoundDrawables()[0],
                    edit.getCompoundDrawables()[1], x, edit.getCompoundDrawables()[3]);
//            edit.setCompoundDrawablesRelativeWithIntrinsicBounds(edit.getCompoundDrawables()[0],
//                    edit.getCompoundDrawables()[1], x, edit.getCompoundDrawables()[3]);
//            edit.setCompoundDrawables(edit.getCompoundDrawables()[0],
//                    edit.getCompoundDrawables()[1], x, edit.getCompoundDrawables()[3]);
        }
    }

    public static String getDurationBreakdown(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        StringBuilder sb = new StringBuilder(64);
        if (days > 0) {
            sb.append(days);
            sb.append(" Days ");
        } else if (hours > 0) {
            sb.append(hours);
            sb.append(" Hours ");
        } else if (minutes > 0) {
            sb.append(minutes);
            sb.append(" Min ");
        } else if (seconds > 0) {
            sb.append(seconds);
            sb.append(" Sec");
        }
        return (sb.toString());
    }

    public static char[] c = new char[]{'k', 'm', 'b', 't'};

    /**
     * Recursive implementation, invokes itself for each factor of a thousand, increasing the class on each invokation.
     *
     * @param n         the number to format
     * @param iteration in fact this is the class from the array c
     * @return a String representing the number n formatted in a cool looking way.
     */
    public static String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000 ? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));

    }

    public static String dateformate(Date date) throws ParseException {
        /*Date d = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH).parse(date);*/
        Format formatter = new SimpleDateFormat("dd MMMM yyyy");
        String dateValue = formatter.format(date);

        return dateValue;
    }

    public static int getYear(Date date) throws ParseException {

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);


        return year;
    }

    public static String getAlphaNumeric(int len) {
        String ALPHA_NUM = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*&#@$%^";
        StringBuffer sb = new StringBuffer(len);
        for (int i = 0; i < len; i++) {
            int ndx = (int) (Math.random() * ALPHA_NUM.length());
            sb.append(ALPHA_NUM.charAt(ndx));
        }
        return sb.toString();
    }
   /* public static  void animateDragged(View view,Context context){
        Animation animation= AnimationUtils.loadAnimation(context, R.anim.shake);
        view.startAnimation(animation);
    }*/

    public static void clearAnimation(View view) {
        view.clearAnimation();
    }

    public static boolean appInstalledOrNot(String uri, Context context) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static String loadJSONFromAsset(Context context, String yourJsonFileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(yourJsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getHour_min_sec(long durationMs) {
        String duration = "";
        if (durationMs > 0) {
            duration = String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(durationMs),
                    TimeUnit.MILLISECONDS.toMinutes(durationMs) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(durationMs)),
                    TimeUnit.MILLISECONDS.toSeconds(durationMs) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationMs)));
        }
        return duration;
    }

    public static String min_sec(long durationMs) {
        String duration = "";
        if (durationMs > 0) {
            duration = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(durationMs) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(durationMs)),
                    TimeUnit.MILLISECONDS.toSeconds(durationMs) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationMs)));
        }
        return duration;
    }

    public static String stringForTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        Formatter mFormatter = new Formatter();
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }


    public static <T> T[] concatenate(T[] a, T[] b) {
        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }


    public static String formatFileSize(long size) {
        if (size < 1024) {
            return String.format("%d B", size);
        } else if (size < 1024 * 1024) {
            return String.format("%.1f KB", size / 1024.0f);
        } else if (size < 1024 * 1024 * 1024) {
            return String.format("%.1f MB", size / 1024.0f / 1024.0f);
        } else {
            return String.format("%.1f GB", size / 1024.0f / 1024.0f / 1024.0f);
        }
    }


    public static File checkFileExist(File file) {
        File newFile=null;

        if(file.exists()) {
            //Do something
            //File file2 = new File(Environment.getExternalStorageDirectory(),File.separator+file1.getName());
            String type_input = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
            String fileName = file.getName().substring(0,file.getName().lastIndexOf("."));
            Log.e("fileName",""+fileName);
            newFile = new File(file.getParent(),fileName+"_"+(System.currentTimeMillis()+1000)+type_input);
            Log.e("newfileName",""+newFile.getAbsolutePath());
            return newFile;
        }
        else {

            /*String type_input = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
            String fileName = file.getName().substring(0,file.getName().lastIndexOf("."));
            Log.e("fileName",""+fileName);
            newFile = new File(file.getParent(),fileName+"_"+System.currentTimeMillis()+type_input);*/
            return file;
        }
    }
    public static Uri getArtUriFromMusicFile(File file, Context context) {
        final Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        final String[] cursor_cols = { MediaStore.Audio.Media.ALBUM_ID };

        final String where = MediaStore.Audio.Media.IS_MUSIC + "=1 AND " + MediaStore.Audio.Media.DATA + " = '"
                + file.getAbsolutePath() + "'";
        final Cursor cursor = context.getContentResolver().query(uri, cursor_cols, where, null, null);
        Log.d("getArtUriFromMusicFile", "Cursor count:" + cursor.getCount());
        /*
         * If the cusor count is greater than 0 then parse the data and get the art id.
         */
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            Long albumId = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));

            Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId);
            cursor.close();
            return albumArtUri;
        }
        return Uri.EMPTY;
    }

    public static long getaAlbumId(File file, Context context) {
        final Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        final String[] cursor_cols = { MediaStore.Audio.Media.ALBUM_ID };

        final String where = MediaStore.Audio.Media.IS_MUSIC + "=1 AND " + MediaStore.Audio.Media.DATA + " = '"
                + file.getAbsolutePath() + "'";
        final Cursor cursor = context.getContentResolver().query(uri, cursor_cols, where, null, null);
        Log.d("getArtUriFromMusicFile", "Cursor count:" + cursor.getCount());
        /*
         * If the cusor count is greater than 0 then parse the data and get the art id.
         */
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            Long albumId = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));

            Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId);
            cursor.close();
            return albumId;
        }
        return 0;
    }

    public static String strSeparator = ",,";
    public static String convertArrayToString(String[] array){
        String str = "";
        for (int i = 0;i<array.length; i++) {
            str = str+array[i];
            // Do not append comma at the end of last element
            if(i<array.length-1){
                str = str+strSeparator;
            }
        }
        return str;
    }
    public static String[] convertStringToArray(String str){
        String[] arr = str.split(strSeparator);
        return arr;
    }
    public static int fetchAccentColor(Context mContext, int attr) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = mContext.obtainStyledAttributes(typedValue.data, new int[] { attr });
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    public static  boolean deleteFile ( File file) {

        boolean deleted = file.delete();
        return deleted;
    }

    public static int getPrimaryColor(Activity activity, int value) {
        //value = R.attr.colorPrimary
        TypedValue typedValue = new TypedValue();
        activity.getTheme().resolveAttribute(value, typedValue, true);
        int color = typedValue.data;
        return color;

    }
    public static int getStatusBarHeight(Activity activity) {
        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static Drawable getChangeColoredDraw(Context context, int resource, int color_type) {
        Resources res = context.getResources();
        Drawable background = ContextCompat.getDrawable(context,resource);
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(color_type, typedValue, true);
        int color = typedValue.data;
        background.setColorFilter(color, PorterDuff.Mode.SRC_IN);

        return background;
    }

    public static Drawable setTint(Drawable d, int color) {
        Drawable wrappedDrawable = DrawableCompat.wrap(d);
        DrawableCompat.setTint(wrappedDrawable, color);
        return wrappedDrawable;
    }

    public static void enableDisableViewGroup(ViewGroup viewGroup,
                                              boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }
    public static void deleteDirectory(File file) {
        if( file.exists() ) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
            file.delete();
        }
    }


    public static Uri getUriFromFilrObject(File inputFile, Context context) {
        Uri fileUri = null;
        if (Build.VERSION.SDK_INT >= 24) {
            fileUri =
                    FileProvider.getUriForFile(context,
                            context.getApplicationContext().getPackageName() + ".fileprovider", inputFile);
        } else {
            fileUri = Uri.fromFile(inputFile);
        }

        return fileUri;
    }

    public static boolean isPackageExisted(Context c, String targetPackage) {

        PackageManager pm = c.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage,
                    PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }
    public static String convertytduration(String ytdate) {
        try {
            String result = ytdate.replace("PT", "").replace("H", ":").replace("M", ":").replace("S", "");
            String arr[] = result.split(":");
            String timeString = ytdate;
            if(arr.length == 3) {
                 timeString = String.format("%d:%02d:%02d", Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));
            } else if(arr.length == 2) {
                timeString = String.format("%02d:%02d",  Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
            } else if(arr.length == 1) {
                timeString = String.format("%02d:%02d",  "00", Integer.parseInt(arr[0]));
            }

            return timeString;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return ytdate;
    }
}


