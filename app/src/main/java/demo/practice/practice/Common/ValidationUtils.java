package demo.practice.practice.Common;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import demo.practice.practice.ApplicationLoader;
import demo.practice.practice.R;

/**
 * Created by root on 13/7/17.
 */

public class ValidationUtils {

    public static <T extends TextView> boolean isInValid(T editText) {
        if (editText == null || editText.getText().toString().trim().length() == 0) {
            return true;
        }
        return false;
    }

   /* public static void fName(EditText editText) {
        editText.setError(ApplicationLoader.applicationContext.getString(R.string.val_fname));
    }

    public static void lName(EditText editText) {
        editText.setError(ApplicationLoader.applicationContext.getString(R.string.val_lname));
    }

    public static void emailEmpty(EditText editText) {
        editText.setError(ApplicationLoader.applicationContext.getString(R.string.val_email));
    }

    public static void password(EditText editText) {
        editText.setError(ApplicationLoader.applicationContext.getString(R.string.val_password));
    }

    public static void passwordMatch(EditText editText) {
        editText.setError(ApplicationLoader.applicationContext.getString(R.string.val_cnfm_password));
    }*/

    /**
     * Validator
     */

    public static boolean isStringFilled(String stringValue, String message, Context context, View rootView) {
        if (stringValue == null || (stringValue != null && stringValue.isEmpty())) {
            Utils.showMessage(message, rootView, context);
            return false;
        } else {
            return true;
        }
    }



    public static boolean isValueNonZero(int value, String message, Context context, View rootView) {
        if (value <= 0) {
            Utils.showMessage(message, rootView, context);
            return false;
        } else {
            return true;
        }
    }

    public static boolean isVauleFilled(AppCompatEditText tv, String message, Context context, View rootView) {
        if (tv != null && tv.getText() != null && tv.getText().toString() != null
                && tv.getText().toString().trim().isEmpty()) {
            Utils.showMessage(message, tv, context);
            return false;
        } else {
            return true;
        }
    }

    public static boolean isRadioSelected(RadioGroup radioGroup, String message, Context context, View rootView) {
        if (radioGroup.getCheckedRadioButtonId() == -1) {
            Utils.showMessage(message, rootView, context);
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidLength(AppCompatEditText et, String message, Context context, View rootView, int length) {
        if (et != null && et.getText() != null && !et.getText().toString().isEmpty()
                && et.getText().toString().length() >= length) {
            return true;
        } else {
            et.requestFocus();
            KeyboardUtils.hideKeyboard(et);
            Utils.showMessage(message, et, context);
            return false;
        }
    }

    public static boolean isBothPasswordSame(AppCompatEditText etPwd, AppCompatEditText etConfirm,
                                             String message, Context context, View rootView) {
        if (etPwd.getText().toString().equals(etConfirm.getText().toString())) {
            return true;
        } else {
            etConfirm.requestFocus();
            KeyboardUtils.hideKeyboard(etPwd);
            Utils.showMessage(message, etPwd, context);
            return false;
        }

    }

    public static boolean isBooleanVauleTrue(boolean booleanValue, View tv, String message, Context context, View rootView) {
        if (!booleanValue) {
            Utils.showMessage(message, tv, context);
            return false;
        } else {
            return true;
        }
    }


    public static boolean isValidEmail(AppCompatEditText et, String message, Context context, View rootView) { // (String target) {
        /*if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }*/
        if (et != null && et.getText() != null
                && et.getText().toString().trim().length() > 0) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(et.getText().toString()).matches()) {
                return true;
            } else {
                et.requestFocus();
                KeyboardUtils.hideKeyboard(et);
                Utils.showMessage(message, et, context);
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isValidWebUrl(AppCompatEditText et, String message, Context context, View rootView) { // (String target) {
        /*if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }*/
        if (et != null && et.getText() != null) {
            if (et.getText().toString().trim().length() <= 0) {
                return true;
            } else if (android.util.Patterns.WEB_URL.matcher(et.getText().toString()).matches()) {
                return true;
            } else {
                et.requestFocus();
                KeyboardUtils.hideKeyboard(et);
                Utils.showMessage(message, et, context);
                return false;
            }
        } else {
            return true;
        }
    }



}
