package demo.practice.practice.WebService;

public interface ResponseCallback<T> {

void onSuccess(T response);

void onFail(String message, String code);
}