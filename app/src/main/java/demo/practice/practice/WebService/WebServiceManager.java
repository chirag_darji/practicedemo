package demo.practice.practice.WebService;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import demo.practice.practice.BaseActivity;
import demo.practice.practice.Common.CommonWidget;
import demo.practice.practice.Model.LoginResponse;
import demo.practice.practice.R;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by umang on 8/12/16.
 */

public class WebServiceManager {

    private static final String TAG = "WebServiceManager";
    private static Context mContext;
    ApiEndpoints mApiService;
    private static WebServiceManager mInstance;

    public static String BASEURL = "https://www.googleapis.com/youtube/v3/";

    public WebServiceManager() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        Request request = original.newBuilder()
                                .method(original.method(), original.body())
                                .build();

                        return chain.proceed(request);
                    }
                });
        /*httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Xpi", "6f3aa56d2b0b28e22a99522ae11b89e9")
                        .addHeader("Id", SPreferences.getInstance(mContext).getStringValue(SPreferences.USER_ID))
                        .addHeader("Accesstoken", SPreferences.getInstance(mContext).getStringValue(SPreferences.ACCESS_TOKEN))
                        .build();
                return chain.proceed(request);
            }
        });*/


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        mApiService = retrofit.create(ApiEndpoints.class);
    }

    public static WebServiceManager getInstance(Context context) {
        if (mInstance == null)
            mInstance = new WebServiceManager();
        mContext = context;
        return mInstance;
    }


    public static HashMap<String, String> getHeaderParams(Context context) {
        HashMap<String, String> map = new HashMap<String, String>();
        /*map.put(Constants.BUILDVERSION, "1.0.0");
        map.put(Constants.DEVICETYPE, "1");*/

      /*  String appTokan = AppSetting.getToken(context);
        if (TextUtils.isEmpty(appTokan)) {
            map.put(Constants.DEVICETOKEN, "7802773b5b3f46f5469853a268a0bb8c1493287522");
        } else {
            map.put(Constants.DEVICETOKEN, appTokan);
        }*/
        return map;
    }

    public void getVideoInfo(BaseActivity context, String url, final ResponseCallback<LoginResponse> responseCallback) {

        if (!CommonWidget.isConnectingToInternet(context)) {
            responseCallback.onFail(context.getString(R.string.internet_error), "");
            return;
        }
        Call<LoginResponse> call = mApiService.getVideoInfo(url);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Log.i(TAG, "response code" + response.code());
                if (response != null && response.code() == 200 && response.body() != null) {
                    responseCallback.onSuccess(response.body());
                } else {
                    responseCallback.onFail("Failed", "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.i(TAG, "on failure " + call.toString());
                responseCallback.onFail("Failed", "");
            }
        });
    }


}
