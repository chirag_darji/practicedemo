package demo.practice.practice.WebService;


import demo.practice.practice.Model.LoginResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;


/**
 * Created by umang on 8/12/16.
 */

public interface ApiEndpoints {

    @GET
    Call<LoginResponse> getVideoInfo(@Url String url);

    /*@FormUrlEncoded
    @POST("login.json")
    Call<LoginResponse> dologin(@HeaderMap HashMap<String, String> headerParams, @Field("email") String email, @Field("password") String password, @Field("build_version") String build_version, @Field("device_type") String device_type, @Field("device_token") String device_token);*/
   /* @POST("login.json")
    Call<LoginResponse> dologin(@Field("email") String email, @Field("password") String password, @Field("build_version") String build_version, @Field("device_type") String device_type, @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST("get-pataients-lists.json")
    Call<PatientListResponse> getpataientlist(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("service_team_id") String service_team_id, @Field("employee_id") String employee_id);

    @POST("get-department-lists.json")
    Call<DepartmentListResponse> getDepartmentList(@HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("search-provider-lists.json")
    Call<SearchProviderResponse> getSearchProviderLists(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id, @Field("provider_name") String provider_name, @Field("department") String department, @Field("room") String room);

    @FormUrlEncoded
    @POST("findPataients.json")
    Call<SearchByRoomResponse> getSearchProviderbyroom(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("search_param") String search_param, @Field("type") String type);

    @FormUrlEncoded
    @POST("findPataients.json")
    Call<SearchByRoomResponse> getSearchProviderbyroom(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("search_param") String search_param, @Field("type") String type, @Field("employee_id") String employee_id);

    @FormUrlEncoded
    @POST("getPatientServiceTeam.json")
    Call<ServiceTeamResponse> getPatientServiceTeam(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id, @Field("employee_id") String employee_id);

    @FormUrlEncoded
    @POST("patientSignoutNotes.json")
    Call<SignoutResponse> getPatientSignoutNotes(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id0);

    @FormUrlEncoded
    @POST("patient-major-events.json")
    Call<MajorEventResponse> getPatientMajorEvents(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST("patientFollowups.json")
    Call<FollowUpResponse> getpatientFollowups(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST("patient-reminders.json")
    Call<ReminderResponse> getpatientReminders(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST("patientHandoff.json")
    Call<HandoffResponse> getpatientHandoff(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id);

    @Multipart
    @POST("createSignoutNote.json")
    Call<AddSignOutNoteResponse> createSignoutNote(@HeaderMap Map<String, String> headers, @Part("hospital_id") RequestBody hospital_id, @Part("patient_id") RequestBody patient_id, @Part("employee_id") RequestBody employee_id, @Part("department_id") RequestBody department_id, @Part MultipartBody.Part file, @Part("date") RequestBody date, @Part("time") RequestBody time, @Part(" duration") RequestBody duration);

    @FormUrlEncoded
    @POST("createFollowups.json")
    Call<AddFollowupResponse> createFollowups(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id, @Field("employee_id") String employee_id, @Field("department_id") String department_id, @Field("content") String content, @Field("date") String date, @Field("time") String time);

    @FormUrlEncoded
    @POST("create-reminder.json")
    Call<AddReminderResponse> createReminders(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id, @Field("employee_id") String employee_id, @Field("department_id") String department_id, @Field("content") String content, @Field("date") String date, @Field("time") String time);

    @Multipart
    @POST("create-major-event.json")
    Call<AddMajorEventResponse> createMajorEvent(@HeaderMap Map<String, String> headers, @Part("hospital_id") RequestBody hospital_id, @Part("patient_id") RequestBody patient_id, @Part("employee_id") RequestBody employee_id, @Part("department_id") RequestBody department_id, @Part MultipartBody.Part file, @Part("date") RequestBody date, @Part("time") RequestBody time, @Part("event") RequestBody event, @Part(" duration") RequestBody duration);

    @FormUrlEncoded
    @POST("getEmployeeDetail.json")
    Call<LoginResponse> getEmployeeDetail(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id);


    @FormUrlEncoded
    @POST("static-pages.json")
    Call<StaticResponse> getstaticPagesData(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("page_id") String page_id);

    @FormUrlEncoded
    @POST("patientViewNotes.json")
    Call<NoteReminderResponse> getpatientViewNotes(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String page_id, @Field("employee_id") String employee_id);

    @FormUrlEncoded
    @POST("setUserNotification.json")
    Call<CommonResponse> setUserNotification(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id, @Field("is_notification") Boolean is_notification);

    @FormUrlEncoded
    @POST("getFirstCallAndAttendingLists.json")
    Call<ConsultantResponse> getFirstCallAndAttendingLists(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id, @Field("type") String type, @Field("department") String department, @Field("sub_department") String sub_department);

    @FormUrlEncoded
    @POST("createNotes.json")
    Call<CommonResponse> createNotes(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id, @Field("employee_id") String employee_id, @Field("content") String content);

    @FormUrlEncoded
    @POST("createEmployeePlans.json")
    Call<CommonResponse> createEmployeePlans(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id, @Field("employee_id") String employee_id, @Field("content") String content, @Field("discharge") String discharge, @Field("place") String place, @Field("timestamp") String timestamp);

    @FormUrlEncoded
    @POST("my-patient-followups.json")
    Call<MyFollowupResponse> getMyPatientfollowups(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id);

    @FormUrlEncoded
    @POST("myPatientNotes.json")
    Call<MySignoutNoteResponse> getMyPatientSignoutNotes(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id);

    @FormUrlEncoded
    @POST("getAllDepartmentSubdepartmentLists.json")
    Call<AllDepartmentResponse> getAllDepartmentSubdepartmentLists(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id);

    @FormUrlEncoded
    @POST("getEmployeeSchedule.json")
    Call<ScheduleResponse> getEmployeeSchedule(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id, @Field("service_team_id") String service_team_id, @Field("date") String date);

    @FormUrlEncoded
    @POST("deleteReminder.json")
    Call<CommonResponse> deleteReminder(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("reminder_id") String reminder_id);

    @FormUrlEncoded
    @POST("getProviderDetail.json")
    Call<ProviderProfileResponse> getProviderDetail(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST("deleteEmployeePataients.json")
    Call<CommonResponse> deleteEmployeePataients(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id, @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST("editPatientDetail.json")
    Call<CommonResponse> editPatientDetail(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("patient_id") String patient_id, @Field("pmh") String pmh, @Field("diagnosed_with") String diagnosed_with, @Field("patient_status") String patient_status);

    @GET("getEmployeeRoleLists.json")
    Call<DepartmentListResponse> getEmployeeRoleLists(@HeaderMap Map<String, String> headers);


    @FormUrlEncoded
    @POST("getAllPatientFollowups.json")
    Call<AllFollowUpResponse> getAllPatientFollowups(@HeaderMap Map<String, String> headers, @Field("hospital_id") String hospital_id, @Field("employee_id") String employee_id, @Field("service_team_id") String service_team_id);*/
}
