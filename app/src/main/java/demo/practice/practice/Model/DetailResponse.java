
package demo.practice.practice.Model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
//@TypeConverters(ItemConverters.class)
public class DetailResponse implements Parcelable
{
    @PrimaryKey(autoGenerate = true)
    public int uniqueId;

    public DetailResponse() {

    }
    public String userId;
    public String firstName;
    public String lastName;
    public String email;


    protected DetailResponse(Parcel in) {
        uniqueId = in.readInt();
        userId = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeString(userId);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetailResponse> CREATOR = new Creator<DetailResponse>() {
        @Override
        public DetailResponse createFromParcel(Parcel in) {
            return new DetailResponse(in);
        }

        @Override
        public DetailResponse[] newArray(int size) {
            return new DetailResponse[size];
        }
    };
}
