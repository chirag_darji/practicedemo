package demo.practice.practice.Model.TypeConverter;

import android.arch.persistence.room.TypeConverter;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class ItemConverters {

    @TypeConverter
    public static List<Object> stringToArticalList(String data) {
        if (data == null || TextUtils.isEmpty(data)) {
            return Collections.emptyList();
        }
        List<Object> articles = new ArrayList<>();
        List<String> artcalString = Arrays.asList(TextUtils.split(data, "\t"));
        Gson gson = new Gson();
        for (String arti : artcalString) {

            Object article = gson.fromJson(arti, Object.class);
            articles.add(article);
        }
        return articles;
    }

    @TypeConverter
    public static String articalListToString(List<Object> articles) {
        if (articles == null) {
            return "";
        }
        Gson gson = new Gson();
        List<String> articalJsonList = new ArrayList<>();
        for (Object article : articles) {
            articalJsonList.add(gson.toJson(article));
        }
        return TextUtils.join("\t ", articalJsonList);
    }
}