package demo.practice.practice;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.v7.app.AppCompatDelegate;

public class ApplicationLoader extends Application {
   
	 public static volatile Context applicationContext = null;

    static {
        AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_NO);
    }
	@Override
    public void onCreate() {
        super.onCreate();
        boolean isDebuggable = 0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE);
		if (!isDebuggable) {
			//Fabric.with(this, new Crashlytics());
		}

        applicationContext = getApplicationContext();
    }
}
