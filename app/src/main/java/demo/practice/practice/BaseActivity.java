package demo.practice.practice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import demo.practice.practice.Databse.CategoryDatabase;

public class BaseActivity extends AppCompatActivity {
    public CategoryDatabase categoryDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categoryDatabase = CategoryDatabase.getAppDatabase(this);
    }
}
