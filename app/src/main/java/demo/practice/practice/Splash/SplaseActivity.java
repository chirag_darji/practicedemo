package demo.practice.practice.Splash;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import java.util.List;

import demo.practice.practice.BaseActivity;
import demo.practice.practice.MainActivity;
import demo.practice.practice.Model.LoginResponse;
import demo.practice.practice.R;
import demo.practice.practice.Registration.LoginActivity;


public class SplaseActivity extends BaseActivity {


    private Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splase);
        LiveData<List<LoginResponse>> liveData;
        liveData = categoryDatabase.loginDao().fetchAllDataLive();
        liveData.observe(this, new Observer<List<LoginResponse>>() {
            @Override
            public void onChanged(@Nullable final List<LoginResponse> loginResponses) {
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(loginResponses.size() > 0) {
                            Intent i = new Intent(SplaseActivity.this,
                                    MainActivity.class);
                            startActivity(i);
                            finish();
                        }else {
                            Intent i = new Intent(SplaseActivity.this,
                                    LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                },1000);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }
}